/*********************************************/
/* BRIQUETTE                                 */
/* A set of standard functions included with */
/* charcoal, do not modify this file         */
/*********************************************/

//Namespacing
var Briquette = Briquette || {};

Briquette = {
    constructor: function () {
        // Detect browser
        this.isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        this.isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

        // set global variables.
        this.bodyTag = $('body');
    },
    init: function () {
        var o = this;
        o.constructor(); //Make all the variables in constructor accessible to the object
        o.addLastChild();
        o.bigTarget();
    },
    addLastChild: function () {
        if($('html').hasClass('ie8')) {
            // find the list child of each parent element listed below and add a class of .last to it.
            $('td, th, li, .col').filter(':last-child').addClass('last');
        }
    },
    bigTarget: function() {
        // any item with the .bt class will find a containing link and apply the href value to the parent thus making non-links clickable.
        // note: Do not place more than one link item in a big target.
        var o = this;
        o.bodyTag.find('.bt').each(function () {
            var href = $(this).find('a').attr('href');
            $(this).bind('click', function (e){
                e.preventDefault();
                window.location = href;
            });
        });
    },
    logScreenSize: function() {
        // Output the screen width (For development only this should be removed when the site is deployed)
        var o = this;
        o.screenLogger = $('<div style="position:absolute;left:5px;top:5px;padding:10px;font-size:12px;background:black;color:#fff;z-index:10000;opacity:0.8"></div>');
        o.screenLogger.appendTo('body');
        setInterval(
            function() {
                o.viewportWidth = $('body').outerWidth(true);
                o.screenLogger.html(o.viewportWidth+'px');
            }, 500
        );
    },
};

$(function() {
    Briquette.init();
});
